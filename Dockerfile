FROM       golang:alpine
LABEL      maintainer="Xiayu.Li<@kaiqi>"

WORKDIR    /go/src/app
COPY       . .

RUN go install -v ./...

ENTRYPOINT ["app"]
